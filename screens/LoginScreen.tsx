import { useNavigation } from '@react-navigation/core'
import React, { useState, useEffect } from 'react'
import { KeyboardAvoidingView, StyleSheet, TextInput, TouchableOpacity, Text, TouchableOpacityBase, View } from 'react-native'
import {auth} from '../FireBase'

const LoginScreen = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [user, setUser] = useState(false);

  const navigation: string = useNavigation()

  useEffect(() => {
      if (user) {
        navigation.navigate("Home","");
        setUser(false);
      }
  }, [user])

        const handleSignUp = () => {

          auth
            .createUserWithEmailAndPassword(email, password)
            .then((userCredentials: { user: any }) => {
              const user = userCredentials.user
              console.log('Registered with:', user.email)
              
            })
            .catch((error: { message: any }) => alert(error.message))
        }
        const handleLogin = () => {
          auth
            .signInWithEmailAndPassword(email, password)
            .then((userCredentials: { user: any }) => {
              const user = userCredentials.user
              console.log('Logged in with:', user.email)
              setUser(true);
            })
            .catch((error: { message: any }) => alert(error.message))
        }


        return (
          
          <KeyboardAvoidingView
            style={styles.container}
            behavior="padding"
          >

            <View>
              <Text style= {styles.header}>todoAPP📋</Text>
            </View>
            <View style={styles.inputContainer}>
              <TextInput
                placeholder="Email"
                value={email}
                onChangeText={text => setEmail(text)}
                style={styles.input} />
              <TextInput
                placeholder="Password"
                value={password}
                onChangeText={text => setPassword(text)}
                style={styles.input}
                secureTextEntry />
            </View>

            <View style={styles.buttonContainer}>
              <TouchableOpacity
                onPress={handleLogin}
                style={styles.button}
              >
                <Text style={styles.buttonText}>Login</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={handleSignUp}
                style={[styles.button, styles.buttonOutline]}
              >
                <Text style={styles.buttonOutlineText}>Register</Text>
              </TouchableOpacity>
            </View>
          </KeyboardAvoidingView>
        )
      }

export default LoginScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 100,
      },
      inputContainer: {
        width: '80%'
      },
      input: {
        backgroundColor: 'white',
        paddingHorizontal: 15,
        paddingVertical: 10,
        borderRadius: 10,
        marginTop: 5,
      },
      buttonContainer: {
        width: '60%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 40,
      },
      button: {
        backgroundColor: 'gray',
        width: '100%',
        padding: 15,
        borderRadius: 10,
        alignItems: 'center',
      },
      buttonOutline: {
        backgroundColor: 'white',
        marginTop: 5,
        borderColor: 'gray',
        borderWidth: 2,
      },
      buttonText: {
        color: 'white',
        fontWeight: '700',
        fontSize: 16,
      },
      buttonOutlineText: {
        color: 'gray',
        fontWeight: '700',
        fontSize: 16,
      },
      header:{
        fontSize: 40,
        color: 'purple',
        alignSelf: 'center',
        marginBottom: 25,


      }

})
