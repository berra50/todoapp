import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View, KeyboardAvoidingView, TextInput, TouchableOpacity, Keyboard} from 'react-native';
import Task from '../components/Task'
import BottomBar from '../components/BottomBar'



export default function App() {
  const [task, setTask] = useState<any | null>(null);
  const [taskItems, setTaskItems] = useState([]);

  const handleAddTask = ():void => {
     setTaskItems([...taskItems, task]);
     setTask("");


  } 
  const completeTask = () =>{
    let itemsCopy = [...taskItems];
    itemsCopy.splice(index, 1);
    setTaskItems(itemsCopy);
  }

  return (
    <View style={styles.container}>
      {/*Task*/}
      
        <View style={styles.taskSection}>
            <Text style={styles.Title}>Today's Task📜</Text>
            {/*Adding Task */}
            
            <KeyboardAvoidingView style={styles.writeTaskArea}>
              <TextInput style={styles.input} placeholder={'Write Your Task'} value={task} onChangeText ={text => setTask(text)} />
              <TouchableOpacity
                onPress={() => handleAddTask()} 
                style={styles.buttonArea}>
                  <Text style={styles.taskText}>Add Your Task ➕ </Text>
              </TouchableOpacity>
          </KeyboardAvoidingView>

            
            <View style={styles.items}>
              {/*Tasks*/}
              {
                taskItems.map((item, index) =>{
                  return (
                    <TouchableOpacity 
                      key={index}
                      onPress= {() => completeTask()}>
                      <Task text={item} />
                    </TouchableOpacity>
                  )
                  
                  
                })
              }
            </View>
        </View>
        <View> 
          <BottomBar />
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E8EAED',
  },
  taskSection:{
    paddingTop: 80,
    paddingHorizontal: 20,
  },
   Title:{
     marginTop: 10,
     fontSize: 23,
     fontWeight: 'bold',
   },
  items:{
    marginTop: 30,
  },
  writeTaskArea:{
    position: 'absolute',
    top: 20,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
},
input:{
    paddingVertical: 15,
    paddingHorizontal: 15,
    backgroundColor: '#fff',
    borderRadius: 40,
    borderColor :'black',
    width: 250,
},
buttonArea:{
    paddingVertical: 20,
    paddingHorizontal: 10,
    borderColor: 'black',
    borderRadius: 30,
    alignItems: 'center',
    backgroundColor: '#FFF',
    marginLeft: 10,
},
taskText: {
    color: 'purple',
    fontSize: 15,
    fontWeight: 'bold',
},
});
function index(index: any, arg1: number) {
  throw new Error('Function not implemented.');
}

