// Import the functions you need from the SDKs you need
import * as firebase from "firebase";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig= {
  apiKey: "AIzaSyBrq5pQWsmsIvb6E3C19eiEeZjsYobBgv4",
  authDomain: "todoa-4e5aa.firebaseapp.com",
  projectId: "todoa-4e5aa",
  storageBucket: "todoa-4e5aa.appspot.com",
  messagingSenderId: "864508956916",
  appId: "1:864508956916:web:0fa87a4bcdd45ebc5d8ea1"
};

// Initialize Firebase
let app;
if(firebase.apps.length === 0) {
  app = firebase.initializeApp(firebaseConfig);

}
else{
  app = firebase.app()
}

const auth = firebase.auth()


export {auth};