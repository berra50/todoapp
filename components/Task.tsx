import React from "react";
import {View, Text, StyleSheet} from 'react-native'

interface Props {
    props: string;
}

const Tasks = (props) =>{
    return(
        <View style={styles.item}>
            <View style={styles.itemLeft}>
                <View style={styles.circle}></View>
                <Text style={styles.Itemtext}>{props.text}</Text>
            </View>
            <View style={styles.circular}></View>
        </View>
    )
}

const styles = StyleSheet.create({
    item: {
        backgroundColor: '#FFF',
        padding: 15,
        borderRadius: 10,
        borderStyle: 'dashed',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        margin: 20,
    },
    itemLeft:{
        flexDirection: "row",
        padding: 15,
        borderRadius: 10,
    },
    circle:{
        width: 24,
        height: 24,
        backgroundColor: 'purple',
        borderRadius: 50,
        opacity: 0.4,
        marginRight: 15,
    },
    Itemtext:{
        maxWidth: '80%',
    },
    circular:{
        width: 12,
        height: 12,
        borderColor: 'pink',
        borderWidth: 2,
        borderRadius: 5,
        
    },



})

export default Tasks