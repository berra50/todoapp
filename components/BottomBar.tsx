import React from 'react';
import { Pressable, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/core'

const BottomBar = () => {
    
    const navigation: string = useNavigation();

    const handleLogin = () =>{
        navigation.navigate("Login","");
    }
    const handleHome = () => {
        navigation.navigate("Home", "");
    }

    return (
      <View style={styles.NavContainer}>
        <View style= {styles.navPart}>
            <TouchableOpacity
                onPress={handleLogin}
                style={styles.IconPart}
              >
                <Text style={styles.bottomText}>📱Login</Text>
              </TouchableOpacity>
            <Pressable style={styles.IconPart}>
                <Pressable
                onPress={handleHome}>
                
                    <Text style={styles.bottomText}>🏠Home</Text>
                </Pressable>
            </Pressable>
        </View>
      </View>
    );
  }


const styles = StyleSheet.create({
  NavContainer: {
      position: 'absolute',
      alignItems: 'center',
      marginTop: 400,
  },
  navPart:{
      flexDirection: 'row',
      backgroundColor: 'gray',
      width: '90%',
      height: 40,
      justifyContent: 'space-evenly',
      borderRadius: 40,
  },
  IconPart: {
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderColor: 'black',
    borderRadius: 10,
    alignItems: 'center',

  },
 
  bottomText:{
      fontSize: 15,
      color: 'white',
  }

});

export default BottomBar;