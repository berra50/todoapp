import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

class Header extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.containerText}>TodoApp</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    height: 100,
    flexDirection: 'row', // row
    backgroundColor: 'gray',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 10,
    paddingRight: 10
  },
  containerText: {
      paddingTop: 30,
      fontSize: 30,
      fontWeight: 'bold',
  }
});

export default Header;